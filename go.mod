module sk

go 1.13

require (
	github.com/imdario/mergo v0.3.8 // indirect
	gitlab.com/radiofrance/kubecli v0.0.0-00010101000000-000000000000
	golang.org/x/oauth2 v0.0.0-20191122200657-5d9234df094c // indirect
	golang.org/x/time v0.0.0-20191024005414-555d28b269f0 // indirect
	gopkg.in/yaml.v2 v2.2.4
	k8s.io/api v0.0.0-20191121015604-11707872ac1c
	k8s.io/apimachinery v0.0.0-20191123013113-aee2c0efe032
	k8s.io/client-go v0.0.0-20191123055820-8d0e6f1b7b78
	k8s.io/utils v0.0.0-20191114200735-6ca3b61696b6 // indirect
)

replace gitlab.com/radiofrance/kubecli => /home/francois/travaux/programming/gopath/src/gitlab.com/radiofrance/kubecli
