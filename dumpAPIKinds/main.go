package main

import (
	// standard library
	"fmt"
	"log"
	"os"

	// external libraries
	yaml "gopkg.in/yaml.v2"

	// kubernetes client-go et al.

	// radiofrance specific
	"gitlab.com/radiofrance/kubecli"
)

type Config struct {
	DumpPath string
}

func main() {
	kube, err := kubecli.New(os.Args[1])
	if err != nil {
		log.Fatalf("can't connect to K8S cluster, reason: %v", err)
	}
	fmt.Printf("current namespace is %s\n", kube.Namespace)
	dynClient, err := NewDynamicClient(kube)
	if err != nil {
		log.Fatalf("%v", err)
	}

	yamlized, err := yaml.Marshal(dynClient.NamespacedKindGVRMap)
	fmt.Printf("%v\n", string(yamlized))
	yamlized2, err := yaml.Marshal(dynClient.ClusterKindGVRMap)
	fmt.Printf("%v\n", string(yamlized2))

	//	kube.ClientSet
}
