package main

import (
	// standard library

	// external libraries

	// kubernetes client-go et al.
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/client-go/discovery"
	"k8s.io/client-go/dynamic"
	// radiofrance specific
	"gitlab.com/radiofrance/kubecli"
)

type DynamicClient struct {
	NamespaceableClients map[string]dynamic.NamespaceableResourceInterface
	ClusterClients       map[string]dynamic.ResourceInterface
	NamespacedKindGVRMap map[string]schema.GroupVersionResource
	ClusterKindGVRMap    map[string]schema.GroupVersionResource
	apiResourceLists     []*metav1.APIResourceList
}

func NewDynamicClient(c *kubecli.KubeCli) (DynamicClient, error) {
	var newclient DynamicClient
	_, resources, err := discovery.ServerGroupsAndResources(c.ClientSet.DiscoveryClient)
	if err != nil {
		return newclient, err
	}
	newclient.apiResourceLists = resources
	newclient.NamespacedKindGVRMap = make(map[string]schema.GroupVersionResource, 0)
	newclient.ClusterKindGVRMap = make(map[string]schema.GroupVersionResource, 0)
	for _, apiResourceList := range resources {
		gv, err := schema.ParseGroupVersion(apiResourceList.GroupVersion)
		if err != nil {
			return newclient, err
		}
		for _, apiResource := range apiResourceList.APIResources {
			curGVR := schema.GroupVersionResource{
				Group:    gv.Group,
				Version:  gv.Version,
				Resource: apiResource.Name,
			}
			if Contains(apiResource.Verbs, "get") {
				if apiResource.Namespaced {
					if _, ok := newclient.NamespacedKindGVRMap[apiResource.Kind]; !ok {
						newclient.NamespacedKindGVRMap[apiResource.Kind] = curGVR

					}
				} else {
					if _, ok := newclient.ClusterKindGVRMap[apiResource.Kind]; !ok {
						newclient.ClusterKindGVRMap[apiResource.Kind] = curGVR

					}
				}
			}
		}
	}
	return newclient, nil
}
